# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

def calculate_average(values):
    if values:
        list_length = len(values)
        list_sum = sum(values)
        return list_sum/list_length
    else:
        return None

numbers = [4,6,2,44,55,4,44,67,87,67]
empty = []
print(calculate_average(empty))
