# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    if values == 0:
        return None
    else:
        return max(values)


numbers = [4,55,66,22,88]
print(max_in_list(numbers))
