# Complete the pad_left function which takes three parameters
#   * a number
#   * the number of characters in the result
#   * a padding character
# and turns the number into a string of the desired length
# by adding the padding character to the left of it
#
# Examples:
#   * number: 10
#     length: 4
#     pad:    "*"
#     result: "**10"
#   * number: 10
#     length: 5
#     pad:    "0"
#     result: "00010"
#   * number: 1000
#     length: 3
#     pad:    "0"
#     result: "1000"
#   * number: 19
#     length: 5
#     pad:    " "
#     result: "   19"

def pad_left(number, length, pad):
    result = str(number) # creates a variable for result that is equal to the number
    # as a string.
    while len(result) < length: # loops while the length or numbers of characters in the result string
        # is less than the character limit set as length.
        result = pad + result
        print(result) # reset the value for
        # result to prepend the last run of the loop with another pad string
    return result

print(pad_left(530,7,'.'))
