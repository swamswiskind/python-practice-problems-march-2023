# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
#   * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"
#   * If it is a workday, the list needs to contain "laptop"
#   * If it is not a workday, the list needs to contain
#     "surfboard"

# below is my solution, but the official solution returns a more dynamic list based on both key values in the function
def gear_for_day(is_workday, is_sunny):
    if is_workday is False and is_sunny is False:
        return nothing_list
    elif is_workday is True:
        return workday_list
    elif is_sunny is True:
        return sunny_list

nothing_list = ["umbrella","jacket"]
workday_list = ["laptop","keycard"]
sunny_list = ["surfboard","sunscreen"]

print(gear_for_day(True, True))
