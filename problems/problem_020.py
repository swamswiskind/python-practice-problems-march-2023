# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.
# below is a solution without actual lists and getting the length of those lists.
def has_quorum(attendees_list, members_list):
    if attendees_list >= (members_list/2):
        return True
    else:
        return False
attendees = 400
members = 300
print(has_quorum(attendees, members))
