# Complete the reverse_dictionary function which has a
# single parameter that is a dictionary. Return a new
# dictionary that has the original dictionary's values
# for its keys, and the original dictionary's keys for
# its values.
#
# Examples:
#   * input:  {}
#     output: {}
#   * input:  {"key": "value"}
#     output: {"value", "key"}
#   * input:  {"one": 1, "two": 2, "three": 3}
#     output: {1: "one", 2: "two", 3: "three"}

def reverse_dictionary(dictionary):
    result = {} # creates a new empty dictionary to return
    for key,value in dictionary.items(): #loops through all items(key and values) in a dictionary
        result[value] = key
        # result[value] takes the value for each item and sets it to the key.
        # = key sets the key that corresponds with each item as its value.
    return result #returns the new dictionary once populated

dict1 = {}
dict2 = {"key": "value"}
dict3 = {"one": 1, "two": 2, "three": 3}

print(reverse_dictionary(dict1))
print(reverse_dictionary(dict2))
print(reverse_dictionary(dict3))
